import java.net.*;
import java.util.ArrayList;

public class ConnectionList {
	static ArrayList<Socket> list= new ArrayList<Socket>();
	
	static void addTo(Socket s) {
		list.add(s);
		updateIndex();
	}
	static void updateIndex(){
		
	}
	static void show() {
		System.out.println("Conn ID |	IP	|  Remote  |");
		int index;
		for(int i=0; i<list.size(); i++) {
			index=i+1;
			Socket sock2= list.get(i);
			System.out.println(index + "       |     "+ sock2.getInetAddress().getHostAddress() + "   |    " + sock2.getPort());
		}
	}
}
